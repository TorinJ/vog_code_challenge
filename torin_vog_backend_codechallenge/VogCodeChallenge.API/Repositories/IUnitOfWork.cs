﻿using VogCodeChallenge.API.Repositories.Repositories.Interfaces;

namespace VogCodeChallenge.API.Repositories
{
    public interface IUnitOfWork
    {
        IDepartmentRepository Departments { get; }
        IEmployeeRepository Employees { get; }

        // Save method
        Task SaveAsync();
    }
}
