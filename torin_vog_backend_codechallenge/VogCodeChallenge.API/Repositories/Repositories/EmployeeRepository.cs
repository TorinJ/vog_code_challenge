﻿using Microsoft.EntityFrameworkCore;
using VogCodeChallenge.API.Models.Entities;
using VogCodeChallenge.API.Repositories.Repositories.Interfaces;

namespace VogCodeChallenge.API.Repositories.Repositories
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly ApplicationDbContext _context;

        public EmployeeRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Employee>> GetAll()
        {
            // Get list of all listings
            var results = await _context.Employees.ToListAsync();

            // Return the results
            return results;
        }
    }
}
