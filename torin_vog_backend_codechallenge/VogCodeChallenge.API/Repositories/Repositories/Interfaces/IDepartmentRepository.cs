﻿using VogCodeChallenge.API.Models.Entities;

namespace VogCodeChallenge.API.Repositories.Repositories.Interfaces
{
    public interface IDepartmentRepository
    {
        Task<Department> GetById(Guid id);
    }
}
