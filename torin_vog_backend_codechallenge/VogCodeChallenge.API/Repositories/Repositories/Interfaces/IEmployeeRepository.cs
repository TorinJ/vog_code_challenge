﻿using VogCodeChallenge.API.Models.Entities;

namespace VogCodeChallenge.API.Repositories.Repositories.Interfaces
{
    public interface IEmployeeRepository
    {
        Task<IEnumerable<Employee>> GetAll();
    }
}
