﻿using Microsoft.EntityFrameworkCore;
using VogCodeChallenge.API.Models.Entities;

namespace VogCodeChallenge.API.Repositories
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Department> Departments => Set<Department>();
        public DbSet<Employee> Employees => Set<Employee>();
    }
}
