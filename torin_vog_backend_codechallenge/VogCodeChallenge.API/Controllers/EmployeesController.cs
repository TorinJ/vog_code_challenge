﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VogCodeChallenge.API.Models.ViewModels;
using VogCodeChallenge.API.Services.Interfaces;

namespace VogCodeChallenge.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;

        public EmployeesController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<EmployeeVM>> Get()
        {
            try
            {
                var result = _employeeService.GetAll();

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("department/{departmentId}")]
        public ActionResult<IEnumerable<EmployeeVM>> Get([FromRoute] Guid departmentId)
        {
            try
            {
                var result = _employeeService.GetAll().Where(employee => employee.DepartmentId == departmentId);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
