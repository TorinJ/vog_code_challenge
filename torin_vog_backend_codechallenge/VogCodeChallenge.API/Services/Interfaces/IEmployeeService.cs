﻿using VogCodeChallenge.API.Models.ViewModels;

namespace VogCodeChallenge.API.Services.Interfaces
{
    public interface IEmployeeService
    {
        IEnumerable<EmployeeVM> GetAll();
        IList<EmployeeVM> ListAll();
    }
}
