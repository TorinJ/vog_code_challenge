﻿using VogCodeChallenge.API.Models.Entities;
using VogCodeChallenge.API.Models.ViewModels;
using VogCodeChallenge.API.Repositories;
using VogCodeChallenge.API.Services.Interfaces;

namespace VogCodeChallenge.API.Services
{
    public class EmployeeService : IEmployeeService
    {
        /* 
         * 
         * In order to switch over to a database, we would need to register the db as a docker container (see commented out code in docker-compose). We would also add a repository layer to the project (see
         * repository classes). This also involves installing the HuGet package: Npgsql.EntityFrameworkCore.PostgreSQL. We could then access the database through the uow structure to get the repository DbSets
         * 
         */

        private readonly IUnitOfWork _uow;

        public EmployeeService(IUnitOfWork uow)
        {
            _uow = uow;
        }
        public IEnumerable<EmployeeVM> GetAll()
        {
            // Setup test data
            var departments = new List<Department>();
            departments.Add(new Department() { Id = new Guid("17cf5ae0-320e-418a-aea8-000000000000"), Address = "11 Developer Ave", Name = "Development", Employees = new List<Employee>() });
            departments.Add(new Department() { Id = new Guid("17cf5ae0-320e-418a-aea8-111111111111"), Address = "11 Merchants Ave", Name = "Marketing", Employees = new List<Employee>() });
            var employees = new List<Employee>();
            employees.Add(new Employee() { Id = new Guid("17cf5ae0-320e-418a-aea8-222222222222"), FirstName = "John", LastName = "Doe", Address = "123 Made Up St", JobTitle = "Backend", DepartmentName = "Development", Department = departments[0] });
            employees.Add(new Employee() { Id = new Guid("17cf5ae0-320e-418a-aea8-333333333333"), FirstName = "Jane", LastName = "Doe", Address = "127 Made Up St", JobTitle = "Backend", DepartmentName = "Development", Department = departments[0] });
            employees.Add(new Employee() { Id = new Guid("17cf5ae0-320e-418a-aea8-444444444444"), FirstName = "James", LastName = "Doe", Address = "300 Made Up St", JobTitle = "Marketer", DepartmentName = "Martketing", Department = departments[1] });
            departments[0].Employees.Add(employees[0]);
            departments[0].Employees.Add(employees[1]);
            departments[1].Employees.Add(employees[2]);

            var models = employees.Select(item => new EmployeeVM(item));
            // Return employees
            return models;
        }

        public IList<EmployeeVM> ListAll()
        {
            // Grab employee data
            var employees = GetAll().ToList();

            // List all employees in console
            foreach (var employee in employees)
            {
                System.Console.WriteLine(employee.FirstName + " " + employee.LastName);
            }

            // Return employee list
            return employees;
        }
    }
}
